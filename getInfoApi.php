<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'config.php';
require_once $config['libpath'].'/D2LAppContextFactory.php';

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['email'])) {
    header('Location: login.php');//redirect to login page to secure the welcome page without login access.
}

$route;
$method;

if (isset($_POST['search_api'])) {
    if ($_POST['search_api'] == 'WhoAmI') {
        $method = 'GET';
        $route = '/d2l/api/lp/'.$config['LP_Version'].'/users/whoami';
    }
    if ($_POST['search_api'] == 'Get Versions') {
        $method = 'GET';
        $route = '/d2l/api/versions/';
    }
    if ($_POST['search_api'] == 'organization info') {
        $method = 'GET';
        $route = '/d2l/api/lp/1.0/organization/info';
    }
    if ($_POST['search_api'] == 'organization-level') {
        $method = 'GET';
        $route = '/d2l/api/lp/1.6/tools/org/';
    }

    $rpta = doValenceRequest($method, $route);
    echo json_encode($rpta);
} else {
    header('Location: welcome.php');
}

function doValenceRequest($verb, $route)
{
    global $config;

    // if (!isset($_SESSION['userId']) || !isset($_SESSION['userKey'])) {
    //     throw new Exception('This function should only be called once the user is logged in.'.
    //                         'It expects the userId and userKey to be stored in $_SESSION.');
    // }
    $userId = $config['userId'];
    $userKey = $config['userKey'];
    session_write_close();

    // Create authContext
    $authContextFactory = new D2LAppContextFactory();
    $authContext = $authContextFactory->createSecurityContext($config['appId'], $config['appKey']);

    // Create userContext
    $hostSpec = new D2LHostSpec($config['host'], $config['port'], $config['scheme']);
    $userContext = $authContext->createUserContextFromHostSpec($hostSpec, $userId, $userKey);

    // Setup cURL
    $ch = curl_init();
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => $verb,

        /* Explicitly turn this on because some versions of cURL (and therefore PHP installations) do
           not default this to true. This verifies the SSL certs that are used to communicate with an
           LMS via HTTPS. It is vitally important that you always do this in production. */
        CURLOPT_SSL_VERIFYPEER => true,

        /* CURLOPT_CAINFO points to a liste of trusted certificates to use for verifying (see above).
           If you are on some platforms (e.g. possibly Windows) you may need to explicitly provide these.
           Your platform may not require this as it may provide a system-wide setting (which you should prefer).
           If you need to explicitly set this get an up-to-date file from http://curl.haxx.se/docs/caextract.html
           and preferably set this up in your php.ini globally. For more information, see
           http://snippets.webaware.com.au/howto/stop-turning-off-curlopt_ssl_verifypeer-and-fix-your-php-config/ */
        // CURLOPT_CAINFO => getcwd().'/cacert.pem',
    );
    curl_setopt_array($ch, $options);

    $tryAgain = true;
    $numAttempts = 0;

    while ($tryAgain && $numAttempts < 5) {
        // Create url for API call
      $uri = $userContext->createAuthenticatedUri($route, $verb);

        curl_setopt($ch, CURLOPT_URL, $uri);

        // Do call
        $response = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $responseCode = $userContext->handleResult($response, $httpCode, $contentType);

        if ($responseCode == 3) {
            $tryAgain = true;
        } else {
            $tryAgain = false;
        }
        ++$numAttempts;
    }
    if ($responseCode == D2LUserContext::RESULT_OKAY) {
        return json_decode($response, true);
    }

    // TODO handle time skew errors

    throw new Exception("Valence API call failed: $httpCode: $response");
}
