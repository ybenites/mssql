<?php

include 'database/db_conection.php';
if (!isset($_SESSION)) {
    session_start();
}

if (!$_SESSION['email']) {
    header('Location: login.php');//redirect to login page to secure the welcome page without login access.
}

if (isset($_POST['table']) || $_SESSION['table']) {
    if (isset($_POST['table'])) {
        $_SESSION['table'] = $_POST['table'];
    }
    $ctable = $_SESSION['table'];
    $sql = "SELECT * FROM $ctable";

    $a_keys = array();
    ?>
    <html>
    <head lang="en">
        <meta charset="UTF-8">
        <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap.css">
        <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap-theme.min.css">
        <title>Login</title>
    </head>
    <body>
    <div class="container-fluid">
      <div class="row ">
        <div class="table-responsive">
        <table class="table table-striped table-hover">
    <?php
    foreach ($conn->query($sql) as $counter => $row) {
        if ($counter == 0) {
            ?>
            <thead>
            <tr class="info">
            <?php
            foreach (array_keys($row) as $head) {
                if (!is_numeric($head)) {
                    $a_keys[] = $head;
                    ?>
                    <th scope="col"><?php echo $head ?></th>
                    <?php

                }
            }
            ?>
          </thead>
          </tr>
          <tbody>
            <?php

        }
        ?>
        <tr>
          <?php
          foreach ($a_keys as $k_head) {
              ?>
              <td><?php echo $row[$k_head] ?></td>
              <?php

          }
        ?>
        </tr>
        <?php

    }
    ?>
  </tbody>
</table>
</div>
</div>
</div>

</body>

</html>
    <?php
    // echo $table;
} else {
    header('Location: welcome.php');
}
