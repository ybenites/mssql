<?php

require dirname(__FILE__).'/extras/Uploader.php';

// Directory where we're storing uploaded images
// Remember to set correct permissions or it won't work
$upload_dir = 'csv/';

$uploader = new FileUpload('uploadfile');

// Handle the upload
$result = $uploader->handleUpload($upload_dir);

if (!$result) {
    exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));
}

$file = $uploader->getFileName();
$a_file_name = explode('.csv', $file);
if (count($a_file_name) > 1) {
    $name_table = addUnderscore($a_file_name[0]);

    readCsv($upload_dir.$file, $name_table);
    echo json_encode(array('success' => true));
  // create columns
}

// $uploader->getFileName()

// read_files('csv');

function read_files($directorio)
{
    $files = array_diff(scandir($directorio), array('.', '..'));
    if (count($files) > 0) {
        foreach ($files as $file) {
            // print_r($file);
            $a_file_name = explode('.csv', $file);

            if (count($a_file_name) > 1) {
                $name_table = addUnderscore($a_file_name[0]);

                readCsv($directorio.'/'.$file, $name_table);
              // create columns
            }
        }
    }
}

function readCsv($file, $name_table)
{
    $csvFile = file($file);
    $head;
    foreach ($csvFile as $counter => $line) {
        $data = str_getcsv($line);
        if ($counter === 0) {
            $head = $data;
            createTable($name_table, $data);
        } else {
            insertDataTable($name_table, $data, $head);
        }
    }
}
function insertDataTable($name_table, $columns, $heads)
{
    include 'database/db_conection.php';

    $sql = "INSERT INTO dbo.$name_table(";
    $id_head;
    foreach ($heads as $c => $head) {
        if ($c != 0) {
            $sql .= ',';
        } else {
            $id_head = addUnderscore($head);
            $id_head = ($id_head != null) ? str_replace('?', '', utf8_decode($id_head)) : $id_head;
        }

        $val = addUnderscore($head);
        $val = ($val != null) ? str_replace('?', '', utf8_decode($val)) : $val;
        $sql .= "$val";
    }
    $sql .= ') VALUES(';
    $nres;
    foreach ($columns as $c => $column) {
        if ($c != 0) {
            $sql .= ',';
        } elseif ($c == 0) {
            $check_data = "SELECT * from dbo.$name_table where $id_head=$column";
            $nres = $conn->query($check_data);
        }

        $val = ($column) ? $column : 'NULL';

        $sql .= "'$val'";
    }
    $sql .= ')';

    if (!$nres->fetchColumn()) {
        $conn->query($sql);
    }
}
function createTable($name_table, $columns)
{
    include 'database/db_conection.php';

    $check_table = "SELECT COUNT(table_name) as 'ntable' FROM information_schema.tables WHERE table_name ='$name_table'";

    foreach ($conn->query($check_table) as $value) {
        if ($value['ntable'] == 0) {
            $sql = "CREATE TABLE dbo.$name_table(";
            foreach ($columns as $count => $val) {
                $newval = addUnderscore($val);
                // echo mb_convert_encoding($newval, 'UCS-2LE', 'UTF-8');
                $newval = str_replace('?', '', utf8_decode($newval));
                if ($count == 0) {
                    $sql .= "$newval int PRIMARY KEY";
                } else {
                    $sql .= ",$newval varchar(100)";
                }
            }
            $sql .= ')';

            $conn->query($sql);
        }
    }
}
function addUnderscore($string_name)
{
    return implode('_', explode(' ', $string_name));
}
