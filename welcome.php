<?php
session_start();

if (!$_SESSION['email']) {
    header('Location: login.php');//redirect to login page to secure the welcome page without login access.
}

?>

<html>
<head>

    <title>
        Registration
    </title>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap.css">
    <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\styles.css">
</head>

<body>
  <div class="container">
<h1>Welcome</h1><br>
<?php
include 'database/db_conection.php';

echo $_SESSION['email'];
?>


  <div class="jumbotron">
    <div class="row" style="padding-top:10px;">
      <div class="col-xs-2">
        <button id="uploadBtn" class="btn btn-large btn-primary">Choose File</button>
      </div>
      <div class="col-xs-10">
        <div id="progressOuter" class="progress progress-striped active" style="display:none;">
          <div id="progressBar" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="padding-top:10px;">
      <div class="col-xs-10">
        <div id="msgBox">
        </div>
      </div>
    </div>
  </div>
  <div class="jumbotron">
    <div class="row" style="padding-top:10px;">
      <form role="form" method="post" action="listdata.php" >
        <div class="form-group">
      <?php
      $sql = 'SELECT * FROM Sys.Tables';
      foreach ($conn->query($sql) as $table) {
          if ($table['name'] != 'users') {
              ?>
          <button type="submit" class="btn btn-info" name="table" value="<?php echo $table['name'] ?>">
            <?php echo $table['name'] ?>
          </button>
          <?php

          }
      }
      ?>
    </div>
    </form>
    </div>
  </div>
  <div class="jumbotron">
    <div class="row" style="padding-top:10px;">
      <h3>Retrieve Information API</h3>
      <form role="form" method="post" action="getInfoApi.php" >
        <div class="form-group">
          <input class="btn btn-default" type="submit" name="search_api" value="Get Versions">
          <input class="btn btn-default" type="submit" name="search_api" value="WhoAmI">
          <input class="btn btn-default" type="submit" name="search_api" value="organization info">
          <input class="btn btn-default" type="submit" name="search_api" value="organization-level">
          <!-- <input class="btn btn-default" type="submit" name="search_api" value="list courses"> -->


        </div>
      </form>
    </div>
  </div>


<h1><a href="logout.php">Logout here</a> </h1>

</div>


<script src="bootstrap-3.2.0-dist\js\SimpleAjaxUploader.js"></script>
<script src="bootstrap-3.2.0-dist\js\custom.js"></script>

</body>

</html>
